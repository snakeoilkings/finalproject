package regio_vinco;

import audio_manager.AudioManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import pacg.PointAndClickGame;
import pacg.PointAndClickGameDataModel;
import xml_utilities.XMLUtilities;
import static regio_vinco.RegioVinco.*;
import xml_utilities.InvalidXMLFileFormatException;


/**
 * This class manages the game data for the Regio Vinco game application. Note
 * that this game is built using the Point & Click Game Framework as its base. 
 * This class contains methods for managing game data and states.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class RegioVincoDataModel extends PointAndClickGameDataModel {
    // THIS IS THE MAP IMAGE THAT WE'LL USE
    private WritableImage mapImage;
    private PixelReader mapPixelReader;
    private PixelWriter mapPixelWriter;
    
    // AND OTHER GAME DATA
    private String regionName;
    private String subRegionsType;
    private String anthemAudio = TRACKED_SONG;
    private HashMap<Color, String> colorToSubRegionMappings;
    private HashMap<String, Color> subRegionToColorMappings;
    private HashMap<String, ArrayList<int[]>> pixels;
    private LinkedList<String> redSubRegions;
    private LinkedList<MovableText> subRegionStack;
    
    private long gameStartTime = -1;
    private int totalRegions = 0;
    private int regionsFound = 0;
    private int regionsLeft = 0;
    private int incorrectGuesses = 0;
    private int highestScore = 0;
    private int currentScore = 0;
    
    private ArrayList<Node> regionsList;
    private String currentMap = THE_WORLD_FILE_PATH;
    private String subPath = "./data/maps/";
    private String continent;
    private String flagPath = DEFAULT_FLAG_PATH;
    private String hoveredRegion = "";
    private String currentHighScore = "";
    private String highScore = "";
    
    private RegioVincoGameState gameState;

    /**
     * Default constructor, it initializes all data structures for managing the
     * sprites, including the map.
     */
    public RegioVincoDataModel() {
	// INITIALIZE OUR DATA STRUCTURES
	colorToSubRegionMappings = new HashMap();
	subRegionToColorMappings = new HashMap();
	subRegionStack = new LinkedList();
	redSubRegions = new LinkedList();
    }
    
    public String getHighScore(){
        return highScore;
    }
    
    public String getAnthemAudio() {
        return anthemAudio;
    }
    
    public String getCurrentMap() {
        return currentMap;
    }
    
    public void setGameState(RegioVincoGameState state) {
        gameState = state;
    }
    
    public RegioVincoGameState getState() {
        return gameState;
    }
    
    public void setMapImage(WritableImage initMapImage) {
        mapImage = initMapImage;
	mapPixelReader = mapImage.getPixelReader();
	mapPixelWriter = mapImage.getPixelWriter();
    }

    public void removeAllButOneFromeStack(RegioVincoGame game) {
	while (subRegionStack.size() > 1) {
	    MovableText text = subRegionStack.removeFirst();
	    String subRegionName = text.getText().getText();
	    game.gameLayer.getChildren().remove(text.getText());
	    game.gameLayer.getChildren().remove(text.getRectangle());
            game.gameLayer.getChildren().remove(text.getImage());
	    this.redSubRegions.clear();
	    
	    // UPDATE GAME STATS
	    regionsLeft = 1;
	    regionsFound = totalRegions - 1;

	    // TURN THE TERRITORY GREEN
	    changeSubRegionColorOnMap(game, subRegionName, BOTTOM_SUB_REGION_FILL_COLOR);
	}
        Color subRegionColor = subRegionToColorMappings.get(subRegionStack.get(0).getText().getText());
        changeSubRegionColorOnMap(game, subRegionStack.get(0).getText().getText(), subRegionColor);
        
	startTextStackMovingDown();
    }

    // ACCESSOR METHODS
    public String getRegionName() {
	return regionName;
    }
    
    public void setCurrentMap(String map) {
        currentMap = map;
    }

    public String getSubRegionsType() {
	return subRegionsType;
    }

    public void setRegionName(String initRegionName) {
	regionName = initRegionName;
    }

    public void setSubRegionsType(String initSubRegionsType) {
	subRegionsType = initSubRegionsType;
    }

    public String getSecondsAsTimeText(long numSeconds) {
	long numHours = numSeconds / 3600;
	numSeconds = numSeconds - (numHours * 3600);
	long numMinutes = numSeconds / 60;
	numSeconds = numSeconds - (numMinutes * 60);

	String timeText = "";
	if (numHours > 0) {
	    timeText += numHours + ":";
	}
	timeText += numMinutes + ":";
	if (numSeconds < 10) {
	    timeText += "0" + numSeconds;
	} else {
	    timeText += numSeconds;
	}
	return timeText;
    }

    public int getRegionsFound() {
	return colorToSubRegionMappings.keySet().size() - subRegionStack.size();
    }

    public int getRegionsNotFound() {
	return subRegionStack.size();
    }
    
    public LinkedList<MovableText> getSubRegionStack() {
	return subRegionStack;
    }
    
    public String getSubRegionMappedToColor(Color colorKey) {
	return colorToSubRegionMappings.get(colorKey);
    }
    
    public Color getColorMappedToSubRegion(String subRegion) {
	return subRegionToColorMappings.get(subRegion);
    }

    // MUTATOR METHODS

    public void addColorToSubRegionMappings(Color colorKey, String subRegionName) {
	colorToSubRegionMappings.put(colorKey, subRegionName);
    }

    public void addSubRegionToColorMappings(String subRegionName, Color colorKey) {
	subRegionToColorMappings.put(subRegionName, colorKey);
    }

    public void respondToMapSelection(RegioVincoGame game, int x, int y) throws IOException {
        // THIS IS WHERE WE'LL CHECK TO SEE IF THE
	// PLAYER CLICKED NO THE CORRECT SUBREGION
        if(gameState != RegioVincoGameState.NAVIGATION){
            Color pixelColor = mapPixelReader.getColor(x, y);
            String clickedSubRegion = colorToSubRegionMappings.get(pixelColor);
            if ((clickedSubRegion == null) || (subRegionStack.isEmpty())) {
                return;
            }
            if (clickedSubRegion.equals(subRegionStack.get(0).getText().getText())) {
                // UPDATE STATS
                regionsLeft--;
                regionsFound++;
	    
                // YAY, CORRECT ANSWER
                if (!game.isFXMuted())
                    game.getAudio().play(SUCCESS, false);

                // TURN THE TERRITORY GREEN
                changeSubRegionColorOnMap(game, clickedSubRegion, BOTTOM_SUB_REGION_FILL_COLOR);

                // REMOVE THE MOVEABLE TEXT FROM THE GAME LAYER
                MovableText mT = subRegionStack.getFirst();
                game.gameLayer.getChildren().remove(mT.getText());
                game.gameLayer.getChildren().remove(mT.getRectangle());
                game.gameLayer.getChildren().remove(mT.getImage());

                // AND REMOVE THE BOTTOM ELEMENT FROM THE STACK
                subRegionStack.removeFirst();
	    
                // AND LET'S CHANGE THE RED ONES BACK TO THEIR PROPER COLORS
                for (String s : redSubRegions) {
                    Color subRegionColor = subRegionToColorMappings.get(s);
                    changeSubRegionColorOnMap(game, s, subRegionColor);
                }
                redSubRegions.clear();

                startTextStackMovingDown();

                if (subRegionStack.isEmpty()) {
                    updateAllStats(game);
                    this.endGameAsWin();
                    ((RegioVincoGame)game).setDialogStatsVisible(true);
                    game.setMapVisible(false);
                    if (currentScore > highestScore)
                        setHighScore();
                    game.getAudio().stop(TRACKED_SONG);
                    if (!game.isMusicMuted())
                        game.getAudio().play(anthemAudio, false);
                    File file = new File(subPath + currentMap + " Flag.png");
                    if (file.exists())
                        game.addVictoryFlag(subPath + currentMap + " Flag.png");
                }
            } else {
                if (!redSubRegions.contains(clickedSubRegion)) {
                    // UPDATE STATS
                    incorrectGuesses++;
		
                    // BOO WRONG ANSWER
                    if (!game.isFXMuted())
                        game.getAudio().play(FAILURE, false);

                    // TURN THE TERRITORY TEMPORARILY RED
                    changeSubRegionColorOnMap(game, clickedSubRegion, WRONG_SUB_REGION_FILL_COLOR);
                    redSubRegions.add(clickedSubRegion);
                }
            }
        }
        else {
            Color pixelColor = mapPixelReader.getColor(x, y);
            String clickedSubRegion = colorToSubRegionMappings.get(pixelColor);
            if (clickedSubRegion == null) {
                return;
            }
            else {
                currentMap = clickedSubRegion;
                game.setNavigationLabel(currentMap);
                subPath+= clickedSubRegion + "/";
                String path = subPath + clickedSubRegion + " Map.png";
                String xmlPath = subPath + clickedSubRegion + " Data.xml";
                navigate(game, path, xmlPath);
            }
            getCurrentHighScore();
        }
    }
    
    public void setHighScore() throws IOException {
        String path = subPath + currentMap + " High Score.txt";
        File file = new File(path);
        if (file.exists()) 
            file.delete();
        PrintWriter out = new PrintWriter(path); 
        out.println(currentScore); 
        out.close();
    }
    
    public void startTextStackMovingDown() {
	// AND START THE REST MOVING DOWN
	for (MovableText mT : subRegionStack) {
	    mT.setVelocityY(SUB_STACK_VELOCITY);
	}
    }

    public void changeSubRegionColorOnMap(RegioVincoGame game, String subRegion, Color color) {
        // THIS IS WHERE WE'LL CHECK TO SEE IF THE
	// PLAYER CLICKED NO THE CORRECT SUBREGION
	ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
	for (int[] pixel : subRegionPixels) {
	    mapPixelWriter.setColor(pixel[0], pixel[1], color);
	}
    }
    
    public String getCurrentMapPath() {
        return subPath + currentMap + " Map.png"; 
    }
    
    public String getCurrentXMLPath() {
        return subPath + currentMap + " Data.xml"; 
    }

    public int getNumberOfSubRegions() {
	return colorToSubRegionMappings.keySet().size();
    }

    /**
     * Resets all the game data so that a brand new game may be played.
     *
     * @param game the RegioVinco game in progress
     */
    @Override
    public void reset(PointAndClickGame game) {
	// STATS
	gameStartTime = new GregorianCalendar().getTimeInMillis();
	totalRegions = 0;
	regionsFound = 0;
	incorrectGuesses = 0;
	
	regionName = "";
	subRegionsType = "";
        
        try {
            getScoreFromFile(subPath + currentMap + " High Score.txt",currentMap);
        } catch (IOException ex) {
            Logger.getLogger(RegioVincoDataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
	// LET'S CLEAR THE DATA STRUCTURES
	colorToSubRegionMappings.clear();
	subRegionToColorMappings.clear();
	subRegionStack.clear();
	redSubRegions.clear();

        // INIT THE MAPPINGS 
        
	for(int i=0; i < regionsList.size(); i++) {
            int red = Integer.valueOf(regionsList.get(i).getAttributes().getNamedItem("red").getNodeValue());
            int blue = Integer.valueOf(regionsList.get(i).getAttributes().getNamedItem("blue").getNodeValue());
            int green = Integer.valueOf(regionsList.get(i).getAttributes().getNamedItem("green").getNodeValue());
            String name = null;
            if (gameState == RegioVincoGameState.NAME_GAME || gameState == RegioVincoGameState.FLAG_GAME)
                if (regionsList.get(i).getAttributes().getNamedItem("name") != null)
                    name = regionsList.get(i).getAttributes().getNamedItem("name").getNodeValue();
                else
                    break;
            if (gameState == RegioVincoGameState.CAPITAL_GAME)
                if (regionsList.get(i).getAttributes().getNamedItem("capital") != null)
                    name = regionsList.get(i).getAttributes().getNamedItem("capital").getNodeValue();
                else
                    break;
            if (gameState == RegioVincoGameState.LEADER_GAME)
                if (regionsList.get(i).getAttributes().getNamedItem("leader") != null)
                    name = regionsList.get(i).getAttributes().getNamedItem("leader").getNodeValue();      
                else
                    break;
            colorToSubRegionMappings.put(makeColor(red, blue, green), name);
        }

	// RESET THE MOVABLE TEXT
	Pane gameLayer = ((RegioVincoGame)game).getGameLayer();
	gameLayer.getChildren().clear();
        int height = 50;
        ArrayList<Color> toBeRemoved = new ArrayList();
        for (Color c : colorToSubRegionMappings.keySet()) {
            if (!(gameState == RegioVincoGameState.FLAG_GAME)) {
                String subRegion = colorToSubRegionMappings.get(c);
                subRegionToColorMappings.put(subRegion, c);
                MovableText subRegionText = new MovableText(gameLayer, 300, height);
                subRegionText.getText().setText(subRegion);
                subRegionText.getText().setFill(SUB_REGION_NAME_COLOR);
                subRegionText.getText().setFont(SUB_REGION_FONT);
                subRegionText.getRectangle().setFill(subRegionToColorMappings.get(subRegion));
                subRegionText.setX(STACK_X);
                subRegionText.setY(0);
                subRegionStack.add(subRegionText);
                totalRegions++;
            }
            else {
                String subRegion = colorToSubRegionMappings.get(c);
                String path = MAPS_PATH +continent+ "/" +subRegion+ "/" +subRegion+ " Flag.png";
                File file = new File(path);
                height = 140;
                if (file.exists()) {
                    Image img = new Image(file.toURI().toString());
                    subRegionToColorMappings.put(subRegion, c);
                    MovableText subRegionText = new MovableText(gameLayer, 200, (int)img.getHeight());
                    subRegionText.setImage(path);
                    subRegionText.getText().setText(subRegion);
                    subRegionText.getRectangle().setFill(subRegionToColorMappings.get(subRegion));
                    subRegionText.setX(STACK_X);
                    subRegionText.setY(0);
                    subRegionStack.add(subRegionText);
                    totalRegions++;
                }
                else 
                    toBeRemoved.add(c);
            }  
        }
	Collections.shuffle(subRegionStack);
        regionsLeft = totalRegions;

        for (int i=0; i < toBeRemoved.size(); i++)
            colorToSubRegionMappings.remove(toBeRemoved.get(i));
        
        int y = STACK_INIT_Y;
	int yInc = STACK_INIT_Y_INC;

	// NOW FIX THEIR Y LOCATIONS
	for (MovableText mT : subRegionStack) {
	    if (gameState == RegioVincoGameState.FLAG_GAME){
                yInc -= mT.getImage().getImage().getHeight();
                int tY = y + yInc;
                mT.setY(tY);
            }
            else {
                int tY = y + yInc;
                mT.setY(tY);
                yInc -= 50;
            }
	}     
        
	// RELOAD THE MAP
        
        // RESET THE AUDIO
	AudioManager audio = ((RegioVincoGame) game).getAudio();
        if (audio.isPlaying(anthemAudio))
            audio.stop(anthemAudio);
        
        String filePath;
        ((RegioVincoGame)game).setWorldNavVisible(false);
        anthemAudio = YOU_WIN;
        if(currentMap.equals("The World")) {
            filePath = THE_WORLD_FILE_PATH;
            ((RegioVincoGame)game).setContinentNavVisible(currentMap, false);
            ((RegioVincoGame)game).setCountryNavVisible(currentMap, false);
        }
        else if (isContinent()) {
            filePath = MAPS_PATH + currentMap +"/"+ currentMap + " Map.png"; 
            ((RegioVincoGame)game).setContinentNavVisible(currentMap, false);
        }
        else {
            //LOAD NATIONAL ANTHEM IF IT EXISTS
            filePath = subPath + currentMap + " Map.png"; 
            File file = new File(subPath + currentMap + " National Anthem.mid");
            if (file.exists()) {
                String anthemAudioPath = subPath + currentMap + " National Anthem.mid";
                anthemAudio = "ANTHEM";
                try {
                    ((RegioVincoGame) game).audio.loadAudio(anthemAudio, anthemAudioPath);
                } catch (Exception e) {
                }
            }
            ((RegioVincoGame)game).setContinentNavVisible(continent, false);
            ((RegioVincoGame)game).setCountryNavVisible(currentMap, false);
        }
        ((RegioVincoGame)game).reloadMap(filePath);

	// LET'S RECORD ALL THE PIXELS
	pixels = new HashMap();
	for (MovableText mT : subRegionStack) {
	    pixels.put(mT.getText().getText(), new ArrayList());
	}

	for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
		Color c = mapPixelReader.getColor(i, j);
		if (colorToSubRegionMappings.containsKey(c)) {
		    String subRegion = colorToSubRegionMappings.get(c);
		    ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
		    int[] pixel = new int[2];
		    pixel[0] = i;
		    pixel[1] = j;
		    subRegionPixels.add(pixel);
		}
	    }
	}
	

	if (!audio.isPlaying(TRACKED_SONG) && !((RegioVincoGame) game).isMusicMuted()) {
	    audio.play(TRACKED_SONG, true);
	}
        
        ((RegioVincoGame) game).disableAll(true);
        //REGION_TITLE = currentMap;
        ((RegioVincoGame)game).setFlagVisible(false);
        ((RegioVincoGame)game).setHighScoreVisible(false);
	// LET'S GO
	beginGame();
    }
   
    // HELPER METHOD FOR MAKING A COLOR OBJECT
    public static Color makeColor(int r, int g, int b) {
	return Color.color(r/255.0, g/255.0, b/255.0);
    }

    // STATE TESTING METHODS
    // UPDATE METHODS
	// updateAll
	// updateDebugText
    
    /**
     * Called each frame, this thread already has a lock on the data. This
     * method updates all the game sprites as needed.
     *
     * @param game the game in progress
     */
    @Override
    public void updateAll(PointAndClickGame game, double percentage) {
            for (MovableText mT : subRegionStack) {
                mT.update(percentage);
            }
            if (!subRegionStack.isEmpty()) {
                MovableText bottomOfStack = subRegionStack.get(0);
                bottomOfStack.getRectangle().setFill(BOTTOM_SUB_REGION_FILL_COLOR);
                bottomOfStack.getText().setFill(BOTTOM_SUB_REGION_TEXT_COLOR);
                double bottomY = bottomOfStack.getText().getY();
                if (gameState == RegioVincoGameState.FLAG_GAME) 
                        bottomY = bottomOfStack.getText().getY()+bottomOfStack.getImage().getImage().getHeight();
                if (bottomY >= FIRST_REGION_Y_IN_STACK) {
                    double diffY = bottomY - FIRST_REGION_Y_IN_STACK;
                    for (MovableText mT : subRegionStack) {
                        mT.setY(mT.getRectangle().getY() - diffY);
                        mT.setVelocityY(0);
                    }
                }
                // UPDATE THE STATS
                updateAllStats((RegioVincoGame)game);
            }
    }
    
    public void getCurrentHighScore() throws IOException {
        String path = subPath + "/" + currentMap + " High Score.txt";
        File file = new File(path);
        if (file.exists()) {
            String text = currentMap + " High Score: 0";
            BufferedReader in;
            try {
                in = new BufferedReader(new FileReader(path));
                text = in.readLine(); 
            } catch (FileNotFoundException ex) {
                Logger.getLogger(RegioVincoDataModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            currentHighScore = currentMap + " High Score: " + text;
        }
        else
            currentHighScore = "No games played for " +currentMap;
    }
    
    public void findFlagPath(RegioVincoGame game, int x, int y) throws IOException {
        Color pixelColor = mapPixelReader.getColor(x, y);
        String region = colorToSubRegionMappings.get(pixelColor);
        if ((region == null) || (regionsList.isEmpty())) {
            highScore = "";
            flagPath = DEFAULT_FLAG_PATH;
        }
        else {
            flagPath = subPath + region + "/" + region + " Flag.png";
            String path = subPath + region + "/" + region + " High Score.txt";
            getScoreFromFile(path, region);
            getCurrentHighScore();
        }
    }
    
    public String getFlagPath() {
        return flagPath;
    }
    
    public String returnCurrentHighScore() {
        return currentHighScore;
    }
    public void getScoreFromFile(String path, String region) throws IOException {
        File file = new File(path);
        if (file.exists()) {
            String text = region + " High Score: 0";
            BufferedReader in;
            try {
                in = new BufferedReader(new FileReader(path));
                text = in.readLine(); 
            } catch (FileNotFoundException ex) {
                Logger.getLogger(RegioVincoDataModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            highScore = region + " High Score: " +  text;
            highestScore = Integer.valueOf(text);
        }
        else {
            highestScore = 0;
            highScore = "No games played for " + region;
        }
    }
    
    public void updateAllStats(RegioVincoGame game) {
	    GregorianCalendar now = new GregorianCalendar();
	    long nowLong = now.getTimeInMillis();
	    long diff = nowLong - gameStartTime;
	    long seconds = diff/1000;
	    String statsText = "" + getSecondsAsTimeText(seconds) 
		    + "      Regions Found: " + regionsFound
		    + "      Regions Left: " + regionsLeft
		    + "      Incorrect Guesses: " + incorrectGuesses;
	    game.updateStatsLabel(statsText);
	    
	    String dialogStatsText = "Region: " + currentMap + "\n"
		    +	"Score: " + calculateScore(seconds) + "\n"
		    +	"Game Duration: " + getSecondsAsTimeText(diff/1000) + "\n"
		    +	"Sub Regions: " + regionsFound + "\n"
		    +	"Incorrect Guesses: " + incorrectGuesses;
	    game.updateDialogStatsLabel(dialogStatsText);	
    }
	
	public int calculateScore(long time) {
	    int score = 10000;
	    score -= (100 * incorrectGuesses);
	    score -= (int)time;
            currentScore = score;
	    return score;
	}
	
    /**
     * Called each frame, this method specifies what debug text to render. Note
     * that this can help with debugging because rather than use a
     * System.out.print statement that is scrolling at a fast frame rate, we can
     * observe variables on screen with the rest of the game as it's being
     * rendered.
     *
     * @return game the active game being played
     */
    public void updateDebugText(PointAndClickGame game) {
	debugText.clear();
    }
    
    public void loadRegions(String xmlPath) {
        
        XMLUtilities util = new XMLUtilities();
        
        File world = new File(xmlPath);
        File worldSchema = new File("./data/" + "WorldSchema.xsd");
        
        Document doc=null;
        
        try {
            doc = util.loadXMLDocument(world.getAbsolutePath(),
                    worldSchema.getAbsolutePath());
        } catch (InvalidXMLFileFormatException ex) {
            Logger.getLogger(RegioVincoDataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        Node regionsListNode = doc.getElementsByTagName("region").item(0);

                
        regionsList = util.getChildNodesWithName(regionsListNode, "sub_region");
        for (int i = 0; i < regionsList.size(); i++)
        {
            // GET THEIR DATA FROM THE DOC
            Node regionNode = regionsList.get(i);
            String leader, capital, red, green, blue, name;
            NamedNodeMap regionAttributes = regionNode.getAttributes();
            if (regionAttributes.getNamedItem("leader") != null)
                leader = regionAttributes.getNamedItem("leader").getNodeValue();
            if (regionAttributes.getNamedItem("name") != null)
                name = regionAttributes.getNamedItem("name").getNodeValue();
            if (regionAttributes.getNamedItem("capital") != null)
                capital = regionAttributes.getNamedItem("capital").getNodeValue();
            if (regionAttributes.getNamedItem("red") != null)
                red = regionAttributes.getNamedItem("red").getNodeValue();
            if (regionAttributes.getNamedItem("green") != null)
                green = regionAttributes.getNamedItem("green").getNodeValue();
            if (regionAttributes.getNamedItem("blue") != null)
                blue = regionAttributes.getNamedItem("blue").getNodeValue();
        }
    }
    
    public void navigate(RegioVincoGame game, String mapPath, String xmlPath) {
        
        loadRegions(xmlPath);
        
        game.setWorldNavVisible(true);
        if (!xmlPath.equals("./data/World.xml") && isContinent()){
            game.setContinentNavVisible(currentMap, true);
            game.setCountryNavVisible("", false);
            continent = currentMap;
        }
        else if(!xmlPath.equals("./data/World.xml") && !isContinent()) {
            game.setContinentNavVisible(continent, true);
            game.setCountryNavVisible(currentMap, true);
        }
        else {
            game.setContinentNavVisible("", false);
            game.setCountryNavVisible("", false);   
            currentMap = "The World";
        }
        
        // LET'S CLEAR THE DATA STRUCTURES
	colorToSubRegionMappings.clear();
	subRegionToColorMappings.clear();
	subRegionStack.clear();
	redSubRegions.clear();

        // INIT THE MAPPINGS 
        game.setFlagValid(false);
        game.setCapitalValid(false);
        game.setLeaderValid(false);
        for(int i=0; i < regionsList.size(); i++) {
            int red = Integer.valueOf(regionsList.get(i).getAttributes().getNamedItem("red").getNodeValue());
            int blue = Integer.valueOf(regionsList.get(i).getAttributes().getNamedItem("blue").getNodeValue());
            int green = Integer.valueOf(regionsList.get(i).getAttributes().getNamedItem("green").getNodeValue());
            String name = regionsList.get(i).getAttributes().getNamedItem("name").getNodeValue();
            colorToSubRegionMappings.put(makeColor(red, blue, green), name);
            
            // Are there any capitals?
            if (regionsList.get(i).getAttributes().getNamedItem("capital") != null)
                game.setCapitalValid(true);
            
            // Are there any leaders?
            if (regionsList.get(i).getAttributes().getNamedItem("leader") != null)
                game.setLeaderValid(true);
            
            // Does a flag exist?
            String path = subPath + "/" +name+ "/" +name+ " Flag.png";
            File file = new File(path);
            if (file.exists())
                game.setFlagValid(true);
        }
        
        // RELOAD THE MAP
	((RegioVincoGame)game).reloadMap(mapPath);

	// LET'S RECORD ALL THE PIXELS
	pixels = new HashMap();
        
        for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
		Color c = mapPixelReader.getColor(i, j);
		if (colorToSubRegionMappings.containsKey(c)) {
		    String subRegion = colorToSubRegionMappings.get(c);
		    ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
		    int[] pixel = new int[2];
		    pixel[0] = i;
		    pixel[1] = j;
                    File file = new File(subPath +  subRegion + "/" + subRegion + " Data.xml");
                    if(!file.exists()){
                          mapPixelWriter.setColor(i, j, Color.PINK);
                    }
		}    
	    }
        }
    }
    
    public void resetSubPath(String path) {
        subPath = "./data/maps/" + path;
    }
    
    public void clearSubRegions(RegioVincoGame game){
        while (subRegionStack.size() > 0) {
	    MovableText text = subRegionStack.removeFirst();
	    String subRegionName = text.getText().getText();
	    game.gameLayer.getChildren().remove(text.getText());
	    game.gameLayer.getChildren().remove(text.getRectangle());
            game.gameLayer.getChildren().remove(text.getImage());
	    this.redSubRegions.clear();
        }
    }

    
    public boolean isContinent() {
        if (currentMap.equals("Europe") || currentMap.equals("Asia") || currentMap.equals("North America")
          || currentMap.equals("Oceania") || currentMap.equals("Antartica") || 
                currentMap.equals("South America")|| currentMap.equals("Africa"))
            return true;
        else
            return false;
    }
}
