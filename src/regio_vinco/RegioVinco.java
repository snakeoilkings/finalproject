package regio_vinco;

import javafx.application.Application;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 * This is the Regio Vinco game application. Note that it extends the
 * PointAndClickGame class and overrides all the proper methods for setting up
 * the Data, the GUI, the Event Handlers, and update and timer task, the thing
 * that actually does the update scheduled rendering.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class RegioVinco extends Application {

    // THESE CONSTANTS SETUP THE GAME DIMENSIONS. THE GAME WIDTH
    // AND HEIGHT SHOULD MIRROR THE BACKGROUND IMAGE DIMENSIONS. WE
    // WILL NOT RENDER ANYTHING OUTSIDE THOSE BOUNDS.

    public static final int GAME_WIDTH = 1200;
    public static final int GAME_HEIGHT = 700;

    // FOR THIS APP WE'RE ONLY PLAYING WITH ONE MAP, BUT
    // IN THE FUTURE OUR GAMES WILL USE LOTS OF THEM
    public static final String REGION_NAME = "Afghanistan";
    public static final String MAPS_PATH = "./data/maps/";
    public static final String AFG_MAP_FILE_PATH = MAPS_PATH + "GreyscaleAFG.png";
    public static final String THE_WORLD_FILE_PATH = MAPS_PATH + "The World Map.png";
    
    // SOME COLORS
    public static final Color MAP_COLOR_KEY = RegioVincoDataModel.makeColor(220, 110, 0);
    public static final Color TRANSPARENT_COLOR = Color.color(0, 0, 0, 0);

    // HERE ARE THE PATHS TO THE REST OF THE IMAGES WE'LL USE
    public static final String GUI_PATH = "./data/gui/";
    public static final String SPLASH_PATH = GUI_PATH + "RegioVincoSplash.png";
    public static final String BACKGROUND_FILE_PATH = GUI_PATH + "RegioVincoBackground.jpg";
    public static final String TITLE_FILE_PATH = GUI_PATH + "RegioVincoTitle.png";
    public static final String START_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoStartButton.png";
    public static final String HELP_BUTTON_FILE_PATH = GUI_PATH + "help.png";
    public static final String HELP_BUTTONS_FILE_PATH = GUI_PATH + "helpButtons.png";
    public static final String SETTINGS_BUTTON_FILE_PATH = GUI_PATH + "settings.png";
    public static final String FLAG_BUTTON_FILE_PATH = GUI_PATH + "flag.png";
    public static final String FLAG_BUTTON_INV_FILE_PATH = GUI_PATH + "flag_invalid.png";
    public static final String DEFAULT_FLAG_PATH = GUI_PATH + "default flag.png";
    public static final String CAPITAL_BUTTON_FILE_PATH = GUI_PATH + "capital.png";
    public static final String CAPITAL_BUTTON_INV_FILE_PATH = GUI_PATH + "capital_invalid.png";
    public static final String WORLD_BUTTON_FILE_PATH = GUI_PATH + "map.png";
    public static final String WORLD_BUTTON_INV_FILE_PATH = GUI_PATH + "map_invalid.png";
    public static final String LEADER_BUTTON_FILE_PATH = GUI_PATH + "leader.png";
    public static final String LEADER_BUTTON_INV_FILE_PATH = GUI_PATH + "leader_invalid.png";
    public static final String HELP_DISPLAY_FILE_PATH = GUI_PATH + "helpsettings.png";
    public static final String BACK_BUTTON_FILE_PATH = GUI_PATH + "back.png";
    public static final String STOP_BUTTON_FILE_PATH = GUI_PATH + "stop.png";
    public static final String STOP_BUTTON_INV_FILE_PATH = GUI_PATH + "stop_invalid.png";
    public static final String START_BUTTON_MO_FILE_PATH = GUI_PATH + "RegioVincoStartButtonMouseOver.png";
    public static final String EXIT_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoExitButton.png";
    public static final String EXIT_BUTTON_MO_FILE_PATH = GUI_PATH + "RegioVincoExitButtonMouseOver.png";
    public static final String SUB_REGION_FILE_PATH = GUI_PATH + "RegioVincoSubRegion.png";
    public static final String WIN_DISPLAY_FILE_PATH = GUI_PATH + "RegioVincoWinDisplay.png";

    // HERE ARE SOME APP-LEVEL SETTINGS, LIKE THE FRAME RATE. ALSO,
    // WE WILL BE LOADING SpriteType DATA FROM A FILE, SO THAT FILE
    // LOCATION IS PROVIDED HERE AS WELL. NOTE THAT IT MIGHT BE A 
    // GOOD IDEA TO LOAD ALL OF THESE SETTINGS FROM A FILE, BUT ALAS,
    // THERE ARE ONLY SO MANY HOURS IN A DAY
    public static final int TARGET_FRAME_RATE = 30;
    public static final String APP_TITLE = "Regio Vinco";
    
    public static final String STOP_INVALID = "STOP_INVALID";
    public static final String CAPITAL_INVALID = "CAPITAL_INVALID";
    public static final String FLAG_INVALID = "FLAG_INVALID";
    public static final String MAP_INVALID = "MAP_INVALID";
    public static final String LEADER_INVALID = "LEADER_INVALID";
    
    // BACKGROUND IMAGE
    public static final String BACKGROUND_TYPE = "BACKGROUND_TYPE";
    public static final int BACKGROUND_X = 0;
    public static final int BACKGROUND_Y = 0;
    
    // TITLE IMAGE
    public static final String TITLE_TYPE = "TITLE_TYPE";
    public static final int TITLE_X = 900;
    public static final int TITLE_Y = 0;
    
    // START GAME BUTTON
    public static final String START_TYPE = "START_TYPE";
    public static final int START_X = 900;
    public static final int START_Y = 100;

    // EXIT GAME BUTTON
    public static final String EXIT_TYPE = "EXIT_TYPE";
    public static final int EXIT_X = 1080;
    public static final int EXIT_Y = 100;
    
    // BACK GAME BUTTON
    public static final String BACK_TYPE = "BACK_TYPE";
    public static final int BACK_X = 975;
    public static final int BACK_Y = 555;
    
    // HELP GAME BUTTON
    public static final String HELP_TYPE = "HELP_TYPE";
    public static final int HELP_X = 1115;
    public static final int HELP_Y = 110;
    
    // VICTORY FLAG IMAGE
    public static final String FLAG_VICTORY_TYPE = "FLAG_VICTORY_TYPE";
    public static final int FLAG_VICTORY_X = 380;
    public static final int FLAG_VICTORY_Y = 185;
    
    // HELP GAME BUTTON
    public static final String STOP_TYPE = "STOP_TYPE";
    public static final int STOP_X = 1160;
    public static final int STOP_Y = 110;
    
    // SPLASH IMAGE
    public static final String SPLASH_TYPE = "SPLASH_TYPE";
    public static final int SPLASH_X = 300;
    public static final int SPLASH_Y = 35;
    
    // SETTINGS BUTTON
    public static final String SETTINGS_TYPE = "SETTINGS_TYPE";
    public static final int SETTINGS_X = 1070;
    public static final int SETTINGS_Y = 110;
    
    // LEADER BUTTON
    public static final String LEADER_TYPE = "LEADER_TYPE";
    public static final int LEADER_X = 1025;
    public static final int LEADER_Y = 110;
    
    
    // MAP BUTTON
    public static final String MAP_ICON_TYPE = "MAP_ICON_TYPE";
    public static final int MAP_ICON_X = 890;
    public static final int MAP_ICON_Y = 110;
    
    // CAPITAL BUTTON
    public static final String CAPITAL_TYPE = "CAPITAL_TYPE";
    public static final int CAPITAL_X = 935;
    public static final int CAPITAL_Y = 110;
    
    // FLAG BUTTON
    public static final String FLAG_TYPE = "FLAG_TYPE";
    public static final int FLAG_X = 980;
    public static final int FLAG_Y = 110;
    
    
    // FOR THE REGION TITLE
    public static String REGION_TITLE = "The World";
    public static final int REGION_TITLE_X = 900;
    public static final int REGION_TITLE_Y = 150;
    public static final Color REGION_COLOR = RegioVincoDataModel.makeColor(220, 220, 80);
    public static final Font REGION_FONT = Font.font("Times New Roman", FontWeight.NORMAL, 32);
    public static final int REGION_TITLE_WIDTH = 300;
    public static final int REGION_TITLE_HEIGHT = 50;
    
    // THE GAME MAP LOCATION
    public static final String MAP_TYPE = "MAP_TYPE";
    public static final String SUB_REGION_TYPE = "SUB_REGION_TYPE";
    public static final int MAP_X = 0;
    public static final int MAP_Y = 0;
    
    // CLOSE VICTORY BUTTON
    public static final String CLOSE_VICTORY = "CLOSE_VICTORY";
    public static final int CLOSE_VICTORY_X= 675;
    public static final int CLOSE_VICTORY_Y = 460;
    
    // MOUSE OVER FLAG IMAGE
    public static final String FLAG_IMAGE_TYPE = "FLAG_IMAGE_TYPE";
    public static final int FLAG_IMAGE_X = 875;
    public static final int FLAG_IMAGE_Y = 265;

    // THE WIN DIALOG
    public static final String WIN_DISPLAY_TYPE = "WIN_DISPLAY";
    public static final int WIN_X = 350;
    public static final int WIN_Y = 150;
    
    // THE HELP DIALOG
    public static final String HELP_DISPLAY_TYPE = "HELP_DISPLAY";
    public static final int HELPSETTINGS_X = 50;
    public static final int HELPSETTINGS_Y = 50;
    
    // THIS IS THE X WHERE WE'LL DRAW ALL THE STACK NODES
    public static final int STACK_X = 900;
    public static final int STACK_INIT_Y = 600;
    public static final int STACK_INIT_Y_INC = 50;

    // STATS
    public static final Color STATS_COLOR = RegioVincoDataModel.makeColor(200, 160, 80);
    public static final Font STATS_FONT = Font.font("Times New Roman", FontWeight.NORMAL, 28);
    public static final int STATS_Y = 650;
    public static final int STATS_X =  10;
    public static final Color DIALOG_STATS_COLOR = RegioVincoDataModel.makeColor(5, 5, 120);
    public static final Font DIALOG_STATS_FONT = Font.font("Times New Roman", FontWeight.NORMAL, 24);
    public static final int DIALOG_STATS_X = 400;
    public static final int DIALOG_STATS_Y = 330;
    
    // FOR THE STACK
    public static final Color SUB_REGION_NAME_COLOR = RegioVincoDataModel.makeColor(10, 10, 100);
    public static final Font SUB_REGION_FONT = Font.font("Times New Roman", FontWeight.NORMAL, 32);
    public static final Color BOTTOM_SUB_REGION_FILL_COLOR = RegioVincoDataModel.makeColor(100, 220, 100);
    public static final Color BOTTOM_SUB_REGION_TEXT_COLOR = Color.RED;
    public static final Color WRONG_SUB_REGION_FILL_COLOR = RegioVincoDataModel.makeColor(240, 160, 160);

    public static final int SUB_STACK_VELOCITY = 5;
    public static final int FIRST_REGION_Y_IN_STACK = GAME_HEIGHT-10;

    public static final String AUDIO_DIR = "./data/audio/";
    public static final String AFGHAN_ANTHEM_FILE_NAME = AUDIO_DIR + "AfghanistanNationalAnthem.mid";
    public static final String SUCCESS_FILE_NAME = AUDIO_DIR + "Success.wav";
    public static final String FAILURE_FILE_NAME = AUDIO_DIR + "Failure.wav";
    public static final String TRACKED_FILE_NAME = AUDIO_DIR + "Tracked.wav";
    public static final String YOU_WIN_FILE_NAME = AUDIO_DIR + "youWin.mid";
    public static final String AFGHAN_ANTHEM = "AFGHAN_ANTHEM";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILURE = "FAILURE";
    public static final String YOU_WIN = "YOU_WIN";
    public static final String TRACKED_SONG = "TRACKED_SONG";
    
    public static final String SOUNDFX = "Sound Effects:                   MUTE";
    public static final String MUSIC =   "Background Music:           MUTE";
    public static final String HELP_BUTTONS = "HELP_BUTTONS";
    public static final int HELP_BUTTONS_X = 100;
    public static final int HELP_BUTTONS_Y = 235;
    public static final String HELP = "\t\t\t\t\tThank you for playing Regio Vinco, a game by Kathryn Blecher.\n"
            + "\tThis game challenges your knowledge of flags, capitals, country location, and country region location.\n\n"
            + "This button starts region name game mode. You match the regions by clicking on the map of the green highlighted\n"
            + "name. It will do so for the map currently navigated to.\n\n" 
            + "This button starts flag mode. Match each flag to their proper country.\n\n"
            + "This button starts capital mode. Match each capital to the proper region.\n\n"
            + "This button starts leader mode. Match each leader to their proper country\n\n"
            + "This is the settings button. Control audio features here.\n\n"
            + "Not all modes are available for all maps! If your button is shaded like this one, that game mode is not available!";

    /**
     * This is where the RegioVinco application starts. It proceeds to make a
     * game and pass it the window, and then starts it.
     *
     * @param primaryStage The window for this JavaFX application.
     */
    @Override
    public void start(Stage primaryStage) {
	RegioVincoGame game = new RegioVincoGame(primaryStage);
	game.startGame();
    }

    /**
     * The RegioVinco game application starts here. All game data and GUI
     * initialization is done through the constructor, so we will just construct
     * our game and set it visible to start it up.
     *
     * @param args command line arguments, which will not be used
     */
    public static void main(String[] args) {
	launch(args);
    }
}
