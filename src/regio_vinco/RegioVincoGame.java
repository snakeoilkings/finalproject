package regio_vinco;

import audio_manager.AudioManager;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import pacg.PointAndClickGame;
import static regio_vinco.RegioVinco.*;
import static regio_vinco.RegioVincoGameState.*;

/**
 * This class is a concrete PointAndClickGame, as specified in The PACG
 * Framework. Note that this one plays Regio Vinco.
 *
 * @author Kathryn Blecher & Mckilla Gorilla
 */
public class RegioVincoGame extends PointAndClickGame {

    // THIS PROVIDES GAME AND GUI EVENT RESPONSES
    RegioVincoController controller;

    // THIS PROVIDES MUSIC AND SOUND EFFECTS
    AudioManager audio;
    
    // THESE ARE THE GUI LAYERS
    Pane backgroundLayer;
    Pane gameLayer;
    Pane guiLayer;
    Pane helpLayer;
    Pane settingsLayer;
    
    // WE'LL SET THESE WHEN WE ENTER THE GAME
    Label regionLabel;
    Label statsLabel;
    Label dialogStatsLabel;
    
    Label worldLabel;
    Label continentLabel;
    Label countryLabel;
    Label countryWinner;
    
    
    Label highScore;
    Label mouseOverHighScore;
    Label helpLabel;
    Label settingsLabel;
    
    Button helpButton;
    Button settingsButton;
    Button backButton;
    Button enterButton;
    Button flagButton;
    Button capitalButton;
    Button mapButton;
    Button leaderButton;
    Button stopButton;
    Button closeButton;
    
    Color mouseOverColor;
    
    Text navText;
    
    boolean flagAdded = false;
    
    ImageView helpView;
    ImageView mapView;
    ImageView mapInvalid;
    ImageView capitalInvalid;
    ImageView flagInvalid;
    ImageView stopInvalid;
    ImageView leaderInvalid;
    ImageView flag;
    ImageView victoryFlag;
    
    Text settingsTextFX;
    Text settingsTextMusic;
    Text helpText;
    CheckBox muteFX;
    CheckBox muteMusic;
    
    Rectangle buttonBackground;
    
    private boolean fxMuted = false;
    private boolean musicMuted = false;
    

    /**
     * Get the game setup.
     */
    public RegioVincoGame(Stage initWindow) {
	super(initWindow, APP_TITLE, TARGET_FRAME_RATE);
	initAudio();
    }
    
    public AudioManager getAudio() {
	return audio;
    }
    
    public Pane getGameLayer() {
	return gameLayer;
    }
    
    public boolean isFXMuted() {
        return fxMuted;
    }
    
    public boolean isMusicMuted() {
        return musicMuted;
    }
    
    public void setFXMuted() {
        if (isFXMuted())
            fxMuted = false;
        else
            fxMuted = true;
    }
    
    public Color getMouseOverColor() {
        return mouseOverColor;
    }
    
    /**
     * Initializes audio for the game.
     */
    private void initAudio() {
	audio = new AudioManager();
	try {
	    audio.loadAudio(TRACKED_SONG, TRACKED_FILE_NAME);
	    audio.play(TRACKED_SONG, true);
            audio.loadAudio(YOU_WIN, YOU_WIN_FILE_NAME);
	    audio.loadAudio(SUCCESS, SUCCESS_FILE_NAME);
	    audio.loadAudio(FAILURE, FAILURE_FILE_NAME);
	} catch (Exception e) {
	    
	}
    }

    // OVERRIDDEN METHODS - REGIO VINCO IMPLEMENTATIONS
    // initData
    // initGUIControls
    // initGUIHandlers
    // reset
    // updateGUI
    /**
     * Initializes the complete data model for this application, forcing the
     * setting of all game data, including all needed SpriteType objects.
     */
    @Override
    public void initData() {
	// INIT OUR DATA MANAGER
	data = new RegioVincoDataModel();
	data.setGameDimensions(GAME_WIDTH, GAME_HEIGHT);

	boundaryLeft = 0;
	boundaryRight = GAME_WIDTH;
	boundaryTop = 0;
	boundaryBottom = GAME_HEIGHT;
    }

    public void setMusicMuted() {
        if (isMusicMuted()) {
            musicMuted = false;
            if (data.won())
                getAudio().play(((RegioVincoDataModel)data).getAnthemAudio(), false);
            else
                getAudio().play(TRACKED_SONG, true);
        }
        else {
            musicMuted = true;
            if (getAudio().isPlaying(TRACKED_SONG))
                getAudio().stop(TRACKED_SONG);
            if (getAudio().isPlaying(((RegioVincoDataModel)data).getAnthemAudio()))
                getAudio().stop(((RegioVincoDataModel)data).getAnthemAudio());
        }
    }
    
    /**
     * For initializing all GUI controls, specifically all the buttons and
     * decor. Note that this method must construct the canvas with its custom
     * renderer.
     */
    @Override
    public void initGUIControls() {
        
        ((RegioVincoDataModel)data).setGameState(NAVIGATION);
	// LOAD THE GUI IMAGES, WHICH INCLUDES THE BUTTONS
	// THESE WILL BE ON SCREEN AT ALL TIMES
	backgroundLayer = new Pane();
	addStackPaneLayer(backgroundLayer);
        addGUIImage(backgroundLayer, BACKGROUND_TYPE, loadImage(BACKGROUND_FILE_PATH), BACKGROUND_X, BACKGROUND_Y);
        backgroundLayer.setVisible(false);
        
        
	// THEN THE GAME LAYER
	gameLayer = new Pane();
	addStackPaneLayer(gameLayer);
	
	// THEN THE GUI LAYER
	guiLayer = new Pane();

	addStackPaneLayer(guiLayer);
         
        buttonBackground = new Rectangle(900, 100, 350, 70);
        buttonBackground.setFill(Color.BLACK);
        buttonBackground.setVisible(false);
        guiLayer.getChildren().add(buttonBackground);
        
        addGUIButton(guiLayer, SETTINGS_TYPE, loadImage(SETTINGS_BUTTON_FILE_PATH), SETTINGS_X, SETTINGS_Y); //1
	addGUIButton(guiLayer, HELP_TYPE, loadImage(HELP_BUTTON_FILE_PATH), HELP_X, HELP_Y); //2
        
        helpButton = this.guiButtons.get(HELP_TYPE);
	helpButton.setBorder(Border.EMPTY);
	helpButton.setPadding(Insets.EMPTY);
	helpButton.setEffect(null);
        helpButton.setVisible(false);
        
        settingsButton = this.guiButtons.get(SETTINGS_TYPE);
	settingsButton.setBorder(Border.EMPTY);
	settingsButton.setPadding(Insets.EMPTY);
	settingsButton.setEffect(null);
        settingsButton.setVisible(false);
      
        
        //Splash Rectangle
        Rectangle splashRec = new Rectangle(25, 25, 1150, 650); 
        splashRec.setFill(Color.BLACK);
        splashRec.setStroke(Color.color(153.0/255.0, 217.0/255.0, 234.0/255.0));
        splashRec.setStrokeWidth(20);
        guiLayer.getChildren().add(splashRec); //3
        
        //Top Corner Image
        addGUIImage(guiLayer, TITLE_TYPE, loadImage(TITLE_FILE_PATH), TITLE_X, TITLE_Y); //4
	ImageView img = this.guiImages.get(TITLE_TYPE);
        img.setVisible(false);
        
        //SPLASH IMAGE
        addGUIImage(guiLayer, SPLASH_TYPE, loadImage(SPLASH_PATH), SPLASH_X, SPLASH_Y); //5
           
        //Splash enter button
        
        enterButton = new Button();
        enterButton.setFont(Font.font("Lucida Handwriting", 24));
        enterButton.setTextFill(Color.WHITE);
        enterButton.setStyle("-fx-background-color:#000000");
        enterButton.setText("Enter Game");
        enterButton.setLayoutX(475);
        enterButton.setLayoutY(595);
        guiLayer.getChildren().add(enterButton); //6
	
        
	// NOW LET'S ADD THE REGION LABEL
	regionLabel = new Label();
	guiLayer.getChildren().add(regionLabel); //7
	regionLabel.translateXProperty().setValue(REGION_TITLE_X);
	regionLabel.translateYProperty().setValue(REGION_TITLE_Y);
	regionLabel.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, null)));
	regionLabel.setTextFill(REGION_COLOR);
	regionLabel.setFont(REGION_FONT);
	regionLabel.setMinWidth(REGION_TITLE_WIDTH);
	regionLabel.setMaxWidth(REGION_TITLE_WIDTH);
	regionLabel.setMinHeight(REGION_TITLE_HEIGHT);
	regionLabel.setMaxHeight(REGION_TITLE_HEIGHT);
        regionLabel.setVisible(false);
	
	// NOTE THAT THE MAP IS ALSO AN IMAGE, BUT
	// WE'LL LOAD THAT WHEN A GAME STARTS, SINCE
	// WE'LL BE CHANGING THE PIXELS EACH TIME
	// FOR NOW WE'LL JUST LOAD THE ImageView
	// THAT WILL STORE THAT IMAGE
	mapView = new ImageView();
	mapView.setX(MAP_X);
	mapView.setY(MAP_Y);
	guiImages.put(MAP_TYPE, mapView);
	guiLayer.getChildren().add(mapView); //8
	
	// IN GAME STATS
	statsLabel = new Label("");
	guiLayer.getChildren().add(statsLabel); //9
	statsLabel.translateXProperty().setValue(STATS_X);
	statsLabel.translateYProperty().setValue(STATS_Y);
	statsLabel.setTextFill(STATS_COLOR);
	statsLabel.setFont(STATS_FONT);
        
        worldLabel = new Label("The World");
	guiLayer.getChildren().add(worldLabel); //10
	worldLabel.translateXProperty().setValue(STATS_X);
	worldLabel.translateYProperty().setValue(STATS_Y);
	worldLabel.setTextFill(STATS_COLOR);
	worldLabel.setFont(STATS_FONT);
        worldLabel.setVisible(false);

        continentLabel = new Label("");
	guiLayer.getChildren().add(continentLabel); //11
	continentLabel.translateXProperty().setValue(STATS_X+135);
	continentLabel.translateYProperty().setValue(STATS_Y);
	continentLabel.setTextFill(STATS_COLOR);
	continentLabel.setFont(STATS_FONT);
        
        countryLabel = new Label("");
	guiLayer.getChildren().add(countryLabel); //12
	countryLabel.translateXProperty().setValue(STATS_X+260);
	countryLabel.translateYProperty().setValue(STATS_Y);
	countryLabel.setTextFill(STATS_COLOR);
	countryLabel.setFont(STATS_FONT);
        countryLabel.setVisible(false);

	// NOW LOAD THE WIN DISPLAY, WHICH WE'LL ONLY
	// MAKE VISIBLE AND ENABLED AS NEEDED
	ImageView winView = addGUIImage(guiLayer, WIN_DISPLAY_TYPE, loadImage(WIN_DISPLAY_FILE_PATH), WIN_X, WIN_Y); //13
	winView.setVisible(false);
        
        // NOW LOAD THE HELP DISPLAY, WHICH WE'LL ONLY
	// MAKE VISIBLE AND ENABLED AS NEEDED
	helpView = addGUIImage(guiLayer, HELP_DISPLAY_TYPE, loadImage(HELP_DISPLAY_FILE_PATH), HELPSETTINGS_X, HELPSETTINGS_Y); //14
	helpView.setVisible(false);
        
        addGUIButton(guiLayer, BACK_TYPE, loadImage(BACK_BUTTON_FILE_PATH), BACK_X, BACK_Y); //15
	backButton = this.guiButtons.get(BACK_TYPE);
	backButton.setBorder(Border.EMPTY);
	backButton.setPadding(Insets.EMPTY);
	backButton.setEffect(null);
        backButton.setVisible(false);
        
        helpLabel = new Label("Help"); 
        helpLabel.setTranslateX(570);
        helpLabel.setTranslateY(100);
        helpLabel.setFont(Font.font("SansSerif", 30));
        helpLabel.setUnderline(true);
        helpLabel.setVisible(false);   
        guiLayer.getChildren().add(helpLabel); //16
        
        navText = new Text();
        navText.setText("The World");  
        navText.setX(400);
        navText.setY(50);
        navText.setFont(Font.font("Comic Sans MS", 35));
        navText.setFill(Color.WHITE);
        navText.setVisible(false);
        guiLayer.getChildren().add(navText); //17
        
        // SETTINGS MENU SET UP
        settingsLabel = new Label("Settings");
        settingsLabel.setTranslateX(570);
        settingsLabel.setTranslateY(100); 
        settingsLabel.setFont(Font.font("SansSerif", 30));
        settingsLabel.setUnderline(true);
        settingsLabel.setVisible(false);   
        guiLayer.getChildren().add(settingsLabel); //18
        settingsTextFX = new Text(420, 250, SOUNDFX);
        settingsTextFX.setFont(Font.font("Times New Roman", 26));
        settingsTextFX.setVisible(false);
        settingsTextMusic = new Text(420, 300, MUSIC);
        settingsTextMusic.setFont(Font.font("Times New Roman", 26));
        settingsTextMusic.setVisible(false);
        muteFX = new CheckBox();
        muteFX.setVisible(false);
        muteMusic = new CheckBox();
        muteMusic.setVisible(false);
        muteFX.setTranslateX(800);
        muteFX.setTranslateY(225);
        muteMusic.setTranslateX(800);
        muteMusic.setTranslateY(275);
        guiLayer.getChildren().addAll(settingsTextFX, settingsTextMusic, muteFX, muteMusic); //19, 20, 21, 22

        helpText = new Text(160, 180, HELP);
        helpText.setVisible(false);
        guiLayer.getChildren().add(helpText); //23
        
        addGUIImage(guiLayer, HELP_BUTTONS, loadImage(HELP_BUTTONS_FILE_PATH), HELP_BUTTONS_X, HELP_BUTTONS_Y); //24
        ImageView help = this.guiImages.get(HELP_BUTTONS);
        help.setVisible(false);
                
	// AND THE STATS IN THE DIALOG
	dialogStatsLabel = new Label();
	guiLayer.getChildren().add(dialogStatsLabel); //25
	dialogStatsLabel.translateXProperty().setValue(DIALOG_STATS_X);
	dialogStatsLabel.translateYProperty().setValue(DIALOG_STATS_Y);
	dialogStatsLabel.setTextFill(DIALOG_STATS_COLOR);
	dialogStatsLabel.setFont(DIALOG_STATS_FONT);
	dialogStatsLabel.setWrapText(true);
	dialogStatsLabel.setVisible(false);
   
        addGUIButton(guiLayer, MAP_ICON_TYPE, loadImage(WORLD_BUTTON_FILE_PATH), MAP_ICON_X, MAP_ICON_Y); //26
        addGUIButton(guiLayer, CAPITAL_TYPE, loadImage(CAPITAL_BUTTON_FILE_PATH), CAPITAL_X, CAPITAL_Y);  //27
	addGUIButton(guiLayer, FLAG_TYPE, loadImage(FLAG_BUTTON_FILE_PATH), FLAG_X, FLAG_Y);              //28
        addGUIButton(guiLayer, LEADER_TYPE, loadImage(LEADER_BUTTON_FILE_PATH), LEADER_X, LEADER_Y);      //29
        
        mapButton = this.guiButtons.get(MAP_ICON_TYPE);
	mapButton.setBorder(Border.EMPTY);
	mapButton.setPadding(Insets.EMPTY);
	mapButton.setEffect(null);
        mapButton.setVisible(false);
        
        capitalButton = this.guiButtons.get(CAPITAL_TYPE);
	capitalButton.setBorder(Border.EMPTY);
	capitalButton.setPadding(Insets.EMPTY);
	capitalButton.setEffect(null);
        capitalButton.setVisible(false);
        
        flagButton = this.guiButtons.get(FLAG_TYPE);
	flagButton.setBorder(Border.EMPTY);
	flagButton.setPadding(Insets.EMPTY);
	flagButton.setEffect(null);
        flagButton.setVisible(false);
        
        leaderButton = this.guiButtons.get(LEADER_TYPE);
	leaderButton.setBorder(Border.EMPTY);
	leaderButton.setPadding(Insets.EMPTY);
	leaderButton.setEffect(null);
        leaderButton.setVisible(false);
        
        addGUIButton(guiLayer, STOP_TYPE, loadImage(STOP_BUTTON_FILE_PATH), STOP_X, STOP_Y); //30
        stopButton = this.guiButtons.get(STOP_TYPE);
        stopButton.setBorder(Border.EMPTY);
	stopButton.setPadding(Insets.EMPTY);
	stopButton.setEffect(null);
        stopButton.setVisible(false);
        
        addGUIButton(guiLayer, CLOSE_VICTORY, loadImage(BACK_BUTTON_FILE_PATH), CLOSE_VICTORY_X, CLOSE_VICTORY_Y); //31
        closeButton = guiButtons.get(CLOSE_VICTORY);
        closeButton.setBorder(Border.EMPTY);
	closeButton.setPadding(Insets.EMPTY);
        closeButton.setVisible(false);
        
        mapInvalid = addGUIImage(guiLayer, MAP_INVALID, loadImage(WORLD_BUTTON_INV_FILE_PATH), MAP_ICON_X, MAP_ICON_Y); //32
        capitalInvalid = addGUIImage(guiLayer, CAPITAL_INVALID, loadImage(CAPITAL_BUTTON_INV_FILE_PATH), CAPITAL_X, CAPITAL_Y); //33
        flagInvalid = addGUIImage(guiLayer, FLAG_INVALID, loadImage(FLAG_BUTTON_INV_FILE_PATH), FLAG_X, FLAG_Y); //34
        leaderInvalid = addGUIImage(guiLayer, LEADER_INVALID, loadImage(LEADER_BUTTON_INV_FILE_PATH), LEADER_X, LEADER_Y); //35
        stopInvalid = addGUIImage(guiLayer, STOP_INVALID, loadImage(STOP_BUTTON_INV_FILE_PATH), STOP_X, STOP_Y); //36
        mapInvalid.setVisible(false);
        capitalInvalid.setVisible(false);
        leaderInvalid.setVisible(false);
        flagInvalid.setVisible(false);
        stopInvalid.setVisible(false);
        
        mouseOverHighScore = new Label("");
        mouseOverHighScore.setLayoutX(875);
        mouseOverHighScore.setLayoutY(415);
        mouseOverHighScore.setTextFill(Color.YELLOW);
	mouseOverHighScore.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 22));
        guiLayer.getChildren().add(mouseOverHighScore); //37
        
        countryWinner = new Label("");
        countryWinner.setLayoutX(600);
        countryWinner.setLayoutY(250);
        countryWinner.setTextFill(Color.BLACK);
        countryWinner.setFont(Font.font("Lucida Handwriting", 28));
        countryWinner.setVisible(false);
        guiLayer.getChildren().add(countryWinner); //38
        
        highScore = new Label("");
        highScore.setLayoutX(575);
        highScore.setLayoutY(STATS_Y);
        highScore.setTextFill(STATS_COLOR);
	highScore.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 28));
        highScore.setVisible(false);
        guiLayer.getChildren().add(highScore); //39
        
        
        addGUIImage(guiLayer, FLAG_IMAGE_TYPE, loadImage(DEFAULT_FLAG_PATH), FLAG_IMAGE_X, FLAG_IMAGE_Y); //40
        ImageView flag = guiImages.get(FLAG_IMAGE_TYPE);
        flag.setVisible(false);

    }
    
    //changes whether the stop button is clickable
    public void setStopValid(boolean vis) {
        stopInvalid.setVisible(!vis);
        guiButtons.get(STOP_TYPE).setVisible(vis);
        
    }
    
    //changes whether the flag button is clickable
    public void setFlagValid(boolean vis) {
        flagInvalid.setVisible(!vis);
        guiButtons.get(FLAG_TYPE).setVisible(vis);
        
    }
    
    //changes whether the capital button is clickable
    public void setCapitalValid(boolean vis) {
        capitalInvalid.setVisible(!vis);
        guiButtons.get(CAPITAL_TYPE).setVisible(vis);
    }
    
    //changes whether the leader button is clickable
    public void setLeaderValid(boolean vis) {
        leaderInvalid.setVisible(!vis);
        guiButtons.get(LEADER_TYPE).setVisible(vis);
    }
    
    //changes whther the map button is clickable
    public void setMapValid(boolean vis) {
        mapInvalid.setVisible(!vis);
        guiButtons.get(MAP_ICON_TYPE).setVisible(vis);
        
    }
    // CHANGES MAP TITLE ON TOP TO CURRENT SELECTION
    public void setNavigationLabel(String region) {
        navText.setText(region);
    }
    
    public void setMapVisible(boolean visible) {
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setVisible(visible);
    }
    
    public void setHelpVisible(boolean vis) {
        helpView.setVisible(vis);
        helpView.toFront();
        backButton.setVisible(vis);
        backButton.toFront();
        helpLabel.setVisible(vis);
        helpLabel.toFront();
        helpText.setVisible(vis);
        helpText.toFront();
        settingsLabel.setVisible(false);
        ImageView img = guiImages.get(HELP_BUTTONS);
        img.setVisible(vis);
        img.toFront();
    }
    
    public void setSettingsVisible(boolean vis) {
        helpView.setVisible(vis);
        helpView.toFront();
        backButton.setVisible(vis);
        backButton.toFront();
        helpLabel.setVisible(false);
        settingsLabel.setVisible(vis);
        settingsLabel.toFront();
        muteFX.setVisible(vis);
        muteFX.toFront();
        muteMusic.setVisible(vis);
        muteMusic.toFront();
        settingsTextFX.setVisible(vis);
        settingsTextFX.toFront();
        settingsTextMusic.setVisible(vis);
        settingsTextMusic.toFront();
    }
    
    // HELPER METHOD FOR LOADING IMAGES
    private Image loadImage(String imagePath) {	
	Image img = new Image("file:" + imagePath);
	return img;
    }
    
    public void setRegionLabel(String region) {
        regionLabel.setText(region);
    }
    
    public void updateStatsLabel(String statsText) {
	statsLabel.setText(statsText);
    }
    
    public void setWorldNavVisible(boolean vis) {
        worldLabel.setVisible(vis);
    }    
    
    public void setContinentNavVisible(String continent, boolean vis) {
        continentLabel.setText("-  " + continent);
        continentLabel.setVisible(vis);
    }
    
     public void setCountryNavVisible(String country, boolean vis) {
        countryLabel.setText("-  " + country);
        countryLabel.setVisible(vis);
    }

    public void updateDialogStatsLabel(String statsText) {
	dialogStatsLabel.setText(statsText);
    }
    
    

    /**
     * For initializing all the button handlers for the GUI.
     */
    @Override
    public void initGUIHandlers() {
	controller = new RegioVincoController(this);
       
        capitalButton = guiButtons.get(CAPITAL_TYPE);
	capitalButton.setOnAction(e -> {
            ((RegioVincoDataModel) data).setGameState(CAPITAL_GAME);
	    controller.processStartGameRequest();
	});
        
        mapButton = guiButtons.get(MAP_ICON_TYPE);
	mapButton.setOnAction(e -> {
            ((RegioVincoDataModel) data).setGameState(NAME_GAME);
	    controller.processStartGameRequest();
	});
        
        leaderButton = guiButtons.get(LEADER_TYPE);
	leaderButton.setOnAction(e -> {
            ((RegioVincoDataModel) data).setGameState(LEADER_GAME);
	    controller.processStartGameRequest();
	});
        
        flagButton = guiButtons.get(FLAG_TYPE);
	flagButton.setOnAction(e -> {
            ((RegioVincoDataModel) data).setGameState(FLAG_GAME);
	    controller.processStartGameRequest();
	});
        
        stopButton = guiButtons.get(STOP_TYPE);
	stopButton.setOnAction(e -> {
	    int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to stop your game and return to navigation?");
            if (response == 0){
                ((RegioVincoDataModel) data).setGameState(RegioVincoGameState.NAVIGATION);
                data.endGameAsLoss();
                mouseOverHighScore.setVisible(true);
                ((RegioVincoDataModel) data).resetSubPath("");
                setCountryNavVisible("", false);
                setContinentNavVisible("", false);
                ((RegioVincoDataModel) data).setCurrentMap("The World/");
                enterNav("The World", THE_WORLD_FILE_PATH, "./data/World.xml");
            }
	});
        
        enterButton = (Button) guiLayer.getChildren().get(6);
	enterButton.setOnAction(e -> {
	    controller.processEnterGameRequest();
            enterButton.setVisible(false);
	});
        
        Button helpButton = (Button) guiLayer.getChildren().get(2);
	helpButton.setOnAction(e -> {
	    setHelpVisible(true);
	});
        
        Button settingsButton = (Button) guiLayer.getChildren().get(1);
	settingsButton.setOnAction(e -> {
	    setSettingsVisible(true);
	});
        
        Label world = (Label) guiLayer.getChildren().get(10);
        world.setOnMouseClicked(e -> {
            ((RegioVincoDataModel) data).resetSubPath("");
            setCountryNavVisible("", false);
            setContinentNavVisible("", false);
            ((RegioVincoDataModel) data).setCurrentMap("The World/");
            enterNav("The World", THE_WORLD_FILE_PATH, "./data/World.xml");
        });
        
        Label continent = (Label) guiLayer.getChildren().get(11);
        continent.setOnMouseClicked(e -> {
            String title = continentLabel.getText().substring(3, continentLabel.getText().length());
            ((RegioVincoDataModel) data).resetSubPath(title + "/");
            ((RegioVincoDataModel) data).setCurrentMap(title);
            String xml = ((RegioVincoDataModel)data).getCurrentXMLPath();
            String path = ((RegioVincoDataModel)data).getCurrentMapPath();
            enterNav(title, path, xml);
        });

	// MAKE THE CONTROLLER THE HOOK FOR KEY PRESSES
	keyController.setHook(controller);

	// SETUP MOUSE PRESSES ON THE MAP
	mapView = guiImages.get(MAP_TYPE);
	mapView.setOnMousePressed(e -> {
            try {
                controller.processMapClickRequest((int) e.getX(), (int) e.getY());
            } catch (IOException ex) {
                Logger.getLogger(RegioVincoGame.class.getName()).log(Level.SEVERE, null, ex);
            }
	});
        
        mapView.setOnMouseMoved(e -> {
             try {
                ((RegioVincoController)controller).processMouseMoveRequest((int) e.getX(), (int) e.getY());
            } catch (IOException ex) {
                Logger.getLogger(RegioVincoGame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        closeButton.setOnAction(e -> {
            reset();
            mouseOverHighScore.setVisible(true);
            if (flagAdded) {
                guiLayer.getChildren().remove(41);
                flagAdded = false;
            }
            String path = ((RegioVincoDataModel)data).getCurrentMapPath();
            String title = ((RegioVincoDataModel)data).getCurrentMap();
            String xml = ((RegioVincoDataModel)data).getCurrentXMLPath();
            enterNav(title, path, xml);
        });
        
        Button backButton = (Button) guiLayer.getChildren().get(15);
	backButton.setOnAction(e -> {
	    setHelpVisible(false);
            setSettingsVisible(false);
	});
        
        CheckBox fxMuted = (CheckBox) guiLayer.getChildren().get(21);
        fxMuted.setOnMouseClicked(e -> {
            setFXMuted(); 
        });
        
        CheckBox musicMuted = (CheckBox) guiLayer.getChildren().get(22);
        musicMuted.setOnMouseClicked(e -> {
            setMusicMuted(); 
        });
     
	// KILL THE APP IF THE USER CLOSES THE WINDOW
	window.setOnCloseRequest(e->{
	    controller.processExitGameRequest();
	});

    }

    /**
     * Called when a game is restarted from the beginning, it resets all game
     * data and GUI controls so that the game may start anew.
     */
    @Override
    public void reset() {
	// IF THE WIN DIALOG IS VISIBLE, MAKE IT INVISIBLE
	setMapVisible(true);
	guiImages.get(WIN_DISPLAY_TYPE).setVisible(false);
	dialogStatsLabel.setVisible(false);

	// AND RESET ALL GAME DATA
	data.reset(this);
	
	// DISPLAY THE REGION TITLE
	regionLabel.setText(REGION_TITLE);
        regionLabel.setVisible(true);
    }

    /**
     * This mutator method changes the color of the debug text.
     *
     * @param initColor Color to use for rendering debug text.
     */
    public static void setDebugTextColor(Color initColor) {
//        debugTextColor = initColor;
    }
    
    public void setHighScoreVisible(boolean vis) {
        highScore.setVisible(vis);
    }
    
    public void setFlagVisible(boolean vis) {
        //guiLayer.getChildren().get(41).setVisible(vis);
        guiLayer.getChildren().get(40).setVisible(vis);
        mouseOverHighScore.setVisible(vis);
    }

    /**
     * Called each frame, this method updates the rendering state of all
     * relevant GUI controls, like displaying win and loss states and whether
     * certain buttons should be enabled or disabled.
     */
    int backgroundChangeCounter = 0;

    @Override
    public void updateGUI() {
	// IF THE GAME IS OVER, DISPLAY THE APPROPRIATE RESPONSE
	if (data.won()) {
	    ImageView winImage = guiImages.get(WIN_DISPLAY_TYPE);
	    winImage.setVisible(true);
            countryWinner.setVisible(true);
            countryWinner.setText(((RegioVincoDataModel)data).getCurrentMap());
	    dialogStatsLabel.setVisible(true);
            setStopValid(false);
            closeButton.setVisible(true);
            setNavigationLabel("");
            statsLabel.setVisible(false);
            regionLabel.setVisible(false);
            if (flagAdded)
                victoryFlag.setVisible(true);
            if (!helpView.isVisible()) {
                closeButton.toFront();
                countryWinner.toFront();
                if (flagAdded)
                    victoryFlag.toFront();
            }
	}    
        if (((RegioVincoDataModel)data).getState()==RegioVincoGameState.NAVIGATION){
            String path = ((RegioVincoDataModel)data).getFlagPath();
            if ((!guiLayer.getChildren().get(3).isVisible()) && (!helpView.isVisible())) {
                try {
                    ((RegioVincoDataModel)data).getCurrentHighScore();
                } catch (IOException ex) {
                    Logger.getLogger(RegioVincoGame.class.getName()).log(Level.SEVERE, null, ex);
                }
                highScore.setText(((RegioVincoDataModel)data).returnCurrentHighScore());
                highScore.setVisible(true);
                flag = guiImages.get(FLAG_IMAGE_TYPE);
                flag.toFront();
                String scoreString = ((RegioVincoDataModel)data).getHighScore();
                mouseOverHighScore.setText(scoreString);
                guiLayer.getChildren().remove(40);
                guiImages.remove(FLAG_IMAGE_TYPE);
                addGUIImage(guiLayer, FLAG_IMAGE_TYPE, loadImage(path), FLAG_IMAGE_X, FLAG_IMAGE_Y);
            }            
        } 
    }
    
    public void addVictoryFlag(String path) {
            addGUIImage(guiLayer, FLAG_VICTORY_TYPE, loadImage(path), FLAG_VICTORY_X, FLAG_VICTORY_Y); 
            victoryFlag = guiImages.get(FLAG_VICTORY_TYPE);
            victoryFlag.setFitWidth(150);
            victoryFlag.setVisible(true);
            flagAdded = true;
        }
    
    //disables all game buttons
    public void disableAll(boolean vis) {
        setCapitalValid(!vis);
        setFlagValid(!vis);
        setLeaderValid(!vis);
        setMapValid(!vis);
        setStopValid(vis);
    }

    public void reloadMap(String map) {
        Image tempMapImage = loadImage(map);
	PixelReader pixelReader = tempMapImage.getPixelReader();
	WritableImage mapImage = new WritableImage(pixelReader, (int) tempMapImage.getWidth(), (int) tempMapImage.getHeight());
	mapView = guiImages.get(MAP_TYPE);
	mapView.setImage(mapImage);
	int numSubRegions = ((RegioVincoDataModel) data).getRegionsFound() + ((RegioVincoDataModel) data).getRegionsNotFound();
	this.boundaryTop = -(numSubRegions * 50);
	
	// MAKE THE OUTER BORDER PIXELS TRANSPARENT
	PixelReader mapReader = mapImage.getPixelReader();
	PixelWriter mapWriter = mapImage.getPixelWriter();
	for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
		Color testColor = mapReader.getColor(i, j);
		if (testColor.equals(MAP_COLOR_KEY)) {
		    mapWriter.setColor(i, j, TRANSPARENT_COLOR);
		}
	    }
	}

	// AND GIVE THE WRITABLE MAP TO THE DATA MODEL
	((RegioVincoDataModel) data).setMapImage(mapImage);
    }
    
    public void setDialogStatsVisible(boolean visible) {
	dialogStatsLabel.setVisible(visible);
    }
    
    public void enterNav(String title, String mapPath, String xmlPath) {
        guiImages.get(SPLASH_TYPE).setVisible(false);
        guiButtons.get(HELP_TYPE).setVisible(true);    
        guiButtons.get(SETTINGS_TYPE).setVisible(true); 
        setStopValid(false);
        guiImages.get(TITLE_TYPE).setVisible(true); 
        guiLayer.getChildren().get(3).setVisible(false);
        guiLayer.getChildren().get(5).setVisible(false);
        guiLayer.getChildren().get(4).setVisible(true);
        setCapitalValid(false);
        setMapValid(true);
        setFlagValid(false);
        setLeaderValid(false);
        backgroundLayer.setVisible(true);
        regionLabel.setVisible(false);
        enterButton.setVisible(false);
        buttonBackground.setVisible(true);
        navText.setVisible(true);
        setNavigationLabel(title);
        ((RegioVincoDataModel) data).clearSubRegions(this);
        updateStatsLabel("");
        closeButton.setVisible(false);
        ((RegioVincoDataModel) data).setGameState(RegioVincoGameState.NAVIGATION);
        ((RegioVincoDataModel) data).navigate(this, mapPath, xmlPath);
    }
}